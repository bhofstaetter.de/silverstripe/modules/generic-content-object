<?php

namespace bhofstaetter\GCO;

use bhofstaetter\FrameworkTweaks\BooleanDropdownField;
use bhofstaetter\FrameworkTweaks\Toolbox;
use SilverStripe\AssetAdmin\Forms\UploadField;
use SilverStripe\Assets\File;
use SilverStripe\CMS\Model\SiteTree;
use SilverStripe\Forms\DropdownField;
use SilverStripe\Forms\TextField;
use SilverStripe\Forms\TreeDropdownField;
use SilverStripe\ORM\DataObject;
use SilverStripe\ORM\Filters\PartialMatchFilter;
use UncleCheese\DisplayLogic\Forms\Wrapper;

class Link extends DataObject
{

    private static string $singular_name = 'Link';
    private static string $plural_name = 'Links';
    private static string $table_name = 'GCO_Link';

    private static array $db = [
        'LinkText' => 'Varchar(255)',
        'Type' => "Enum('page,url,email,phone,file', 'page')",
        'TargetBlank' => 'Boolean',
        'Anchor' => 'Varchar(255)',
        'Query' => 'Varchar(255)',
        'URL' => 'Varchar(255)',
        'Email' => 'Varchar(255)',
        'Phone' => 'Varchar(255)',
    ];

    private static array $has_one = [
        'Page' => SiteTree::class,
        'File' => File::class,
    ];

    private static array $belongs_many_many = [
        'ContentObjects' => ContentObject::class,
    ];

    private static array $owns = [
        'File',
    ];

    private static $summary_fields = [
        'LinkText',
        'Type.Nice',
        'getNiceTarget' => 'Ziel',
        'TargetBlank.Nice',
        'Anchor',
        'Query',
        'ContentObjects.Count' => 'Verwendungen',
    ];

    private static $summary_fields_sorting = [
        'Type.Nice' => 'Type',
        'TargetBlank.Nice' => 'TargetBlank',
    ];

    private static $searchable_fields = [
        'LinkText',
        'Type',
        'TargetBlank',
        'Anchor',
        'Query',
        'URL',
        'Email',
        'Phone',
        'Page.Title' => [
            'title' => 'Seitenname',
            'filter' => PartialMatchFilter::class,
        ],
        'Page.MenuTitle' => [
            'title' => 'Navigationsbezeichnung',
            'filter' => PartialMatchFilter::class,
        ],
        'File.Title' => [
            'title' => 'Dateititel',
            'filter' => PartialMatchFilter::class,
        ],
        'File.Filename' => [
            'title' => 'Dateiname',
            'filter' => PartialMatchFilter::class,
        ],
    ];

    public function getCMSFields()
    {
        $fields = $this->getBasicCMSFields();

        $fields->addFieldsToTab('Root.Main', [
            DropdownField::create('Type', 'Typ', $this->dbObject('Type')->niceEnumValues()),
            $page = Wrapper::create(TreeDropdownField::create('PageID', 'Seite', SiteTree::class)),
            $file = UploadField::create('File', 'Datei')
                ->setFolderName('files'),
            $url = TextField::create('URL', 'URL'),
            $email = TextField::create('Email', 'E-Mail'),
            $phone = TextField::create('Phone', 'Telefonnummer'),
            $text = TextField::create('LinkText', 'Linktext'),
            $targetBlank = BooleanDropdownField::create('TargetBlank', 'In neuem Tab öffnen'),
            $anchor = TextField::create('Anchor', 'Anker'),
            $query = TextField::create('Query', 'URL Parameter'),
        ]);

        $targetBlank
            ->displayIf('Type')->isEqualTo('page')
            ->orIf('Type')->isEqualTo('url')
            ->orIf('Type')->isEqualTo('file')
            ->end();

        $anchor
            ->displayIf('Type')->isEqualTo('page')
            ->end();

        $query
            ->displayIf('Type')->isEqualTo('page')
            ->end();

        $page
            ->displayIf('Type')->isEqualTo('page')
            ->end();

        $file
            ->displayIf('Type')->isEqualTo('file')
            ->end();

        $url
            ->displayIf('Type')->isEqualTo('url')
            ->end();

        $email
            ->displayIf('Type')->isEqualTo('email')
            ->end();

        $phone
            ->displayIf('Type')->isEqualTo('phone')
            ->end();

        $this->extend('updateCMSFields', $fields);

        return $fields;
    }

    public function getTarget($titleInsteadOfLink = false)
    {
        switch ($this->Type) {
            case 'page':
                return $titleInsteadOfLink ? $this->Page()->MenuTitle : $this->Page()->Link();
            case 'url':
                return $titleInsteadOfLink ? str_replace(['http://', 'https://', 'www.'], '', $this->URL) : Toolbox::url_add_scheme($this->URL);
            case 'email':
                return $titleInsteadOfLink ? $this->Email : 'mailto:' . $this->Email;
            case 'phone':
                return $titleInsteadOfLink ? $this->Phone : 'tel:' . Toolbox::clean_phone_number($this->Phone);
            case 'file':
                return $titleInsteadOfLink ? $this->File()->Title : $this->File()->Link();
        }
    }

    public function getNiceTarget()
    {
        return $this->getTarget(true);
    }

    public function getTitle()
    {
        $parts = [
            $this->LinkText,
            '[' . $this->dbObject('Type')->Nice() . ':',
            $this->getNiceTarget() . ']',
        ];

        return implode(' ', $parts);
    }
}
