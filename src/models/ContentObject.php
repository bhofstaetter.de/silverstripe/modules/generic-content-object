<?php

namespace bhofstaetter\GCO;

use bhofstaetter\FrameworkTweaks\BooleanDropdownField;
use bhofstaetter\FrameworkTweaks\GridFieldConfig_Extended;
use bhofstaetter\FrameworkTweaks\NoticeField;
use bhofstaetter\FrameworkTweaks\Toolbox;
use Bummzack\SortableFile\Forms\SortableUploadField;
use SilverStripe\AssetAdmin\Forms\UploadField;
use SilverStripe\Assets\File;
use SilverStripe\Assets\Image;
use SilverStripe\Core\ClassInfo;
use SilverStripe\Forms\DropdownField;
use SilverStripe\Forms\FieldList;
use SilverStripe\Forms\Form;
use SilverStripe\Forms\GridField\GridField;
use SilverStripe\Forms\GridField\GridFieldDetailForm;
use SilverStripe\Forms\HTMLEditor\HTMLEditorField;
use SilverStripe\Forms\TextField;
use SilverStripe\ORM\DataObject;
use SilverStripe\ORM\HiddenClass;
use SilverStripe\ORM\ManyManyList;
use SilverStripe\ORM\UnsavedRelationList;
use SilverStripe\ORM\ValidationResult;
use SilverStripe\View\ArrayData;

class ContentObject extends DataObject implements HiddenClass
{
    private static string $singular_name = 'Inhalt';
    private static string $plural_name = 'Inhalte';
    private static string $table_name = 'GCO_ContentObject';

//    private static $belongs_many_many = [
//        '' => '',
//    ];

    public function getCMSFields()
    {
        $fields = $this->getBasicCMSFields(true, true);
        $this->extend('updateCMSFields', $fields);
        return $fields;
    }
}
