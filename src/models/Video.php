<?php

namespace bhofstaetter\GCO;

use bhofstaetter\FrameworkTweaks\NoticeField;
use SilverStripe\AssetAdmin\Forms\UploadField;
use SilverStripe\Assets\File;
use SilverStripe\Assets\Image;
use SilverStripe\Forms\DropdownField;
use SilverStripe\Forms\TextField;
use SilverStripe\ORM\DataObject;
use SilverStripe\ORM\ValidationResult;
use UncleCheese\DisplayLogic\Forms\Wrapper;

class Video extends DataObject
{
    private static string $singular_name = 'Video';
    private static string $plural_name = 'Videos';
    private static string $table_name = 'GCO_Video';

    private static array $db = [
        'Name' => 'Varchar(255)',
        'Provider' => "Enum('youtube,vimeo,file','youtube')",
        'SourceLink' => 'Varchar(255)',
        'SourceID' => 'Varchar(255)',
    ];

    private static array $has_one = [
        'Mp4' => File::class,
        'Webm' => File::class,
        'PreviewImage' => Image::class,
    ];

    private static array $belongs_many_many = [
        'ContentObjects' => ContentObject::class,
    ];

    private static array $owns = [
        'Mp4',
        'Webm',
        'PreviewImage',
    ];

    private static array $required_fields = [
        'Name',
        'Provider',
    ];

    private static array $duplicate_check_conditions = [
        ['Provider', 'Mp4ID', 'WebmID', 'IGNORE_EMPTY'],
        ['Provider', 'SourceLink', 'IGNORE_EMPTY'],
    ];

    private static array $summary_fields = [
        'PreviewImage.CMSThumbnail' => 'Vorschaubild',
        'Title' => 'Titel',
        'Provider.Nice',
    ];

    private static array $summary_fields_sorting = [
        'Title' => 'Name',
        'Provider.Nice' => 'Provider',
    ];

    private static array $searchable_fields = [
        'Name',
    ];

    public function onBeforeWrite()
    {
        parent::onBeforeWrite();
        $this->parseSourceID();
    }

    public function validate(): ValidationResult
    {
        $result = parent::validate();

        $this->validateSourceLink($result);
        $this->validateFiles($result);

        $this->extend('updateValidate', $result);

        return $result;
    }

    private function validateSourceLink(ValidationResult $result): void {
        if ($this->Provider === 'file') {
            return;
        }

        if (!$this->SourceLink) {
            $result->addFieldError('SourceLink', '"Link zum Video" ist ein Pflichtfeld');
        }

        if ($this->SourceLink && !$this->parseSourceID(true)) {
            $result->addFieldError('SourceLink', 'Der Link ist falsch oder das Video nicht zum Einbinden geeignet');
        }
    }

    private function validateFiles(ValidationResult $result): void {
        if ($this->Provider !== 'file') {
            return;
        }

        if (!$this->Mp4()->exists() && !$this->Webm()->exists()) {
            $msg = 'Das Video muss mindestens in einem der zwei Formate hochgeladen werden';

            $result->addFieldError('Mp4', $msg);
            $result->addFieldError('Webm', $msg);
        }
    }

    public function getCMSFields()
    {
        $fields = $this->getBasicCMSFields();

        $fields->addFieldsToTab('Root.Main', [
            DropdownField::create('Provider', 'Quelle', $this->dbObject('Provider')->niceEnumValues())
                ->setEmptyString('(Bitte auswählen)'),
            $sourceLink = TextField::create('SourceLink', 'Link zum Video'),
            $mp4 = UploadField::create('Mp4', 'Video als MP4')
                ->setFolderName($this->getVideoFolderName())
                ->setDescription('Erlaubtes Format: mp4')
                ->setAllowedExtensions(['mp4']),
            $webm = UploadField::create('Webm', 'Video als WEBM')
                ->setFolderName($this->getVideoFolderName())
                ->setDescription('Erlaubtes Format: webm')
                ->setAllowedExtensions(['webm']),
            $fileNotice = Wrapper::create(NoticeField::create(
                'FileNotice',
                'Um alle Geräte zu unterstützen, sollte das Video sowohl im MP4 als auch im WEBM Format hochgeladen werden.',
                $this->Provider === 'file',
                NoticeField::NOTICE,
                'Hinweis'
            )),
            UploadField::create('PreviewImage', 'Vorschaubild')
                ->setFolderName($this->getVideoPreviewImageFolderName())
        ]);

        $sourceLink->displayIf('Provider')->isNotEqualTo('file')->end();
        $sourceLink->setDescription('Link zu einem öffentlichen Video auf Youtube oder Vimeo.');

        $mp4->displayIf('Provider')->isEqualTo('file')->end();
        $webm->displayIf('Provider')->isEqualTo('file')->end();
        $fileNotice->displayIf('Provider')->isEqualTo('file')->end();

        $this->extend('updateCMSFields', $fields);

        return $fields;
    }

    public function getVideoFolderName(): string {
        return 'videos'; // todo
    }

    public function getVideoPreviewImageFolderName(): string {
        return 'videos/preview'; // todo
    }

    private function parseSourceID($returnID = false)
    {
        $provider = $this->Provider;

        if (
            $this->isChanged('SourceLink', 2)
            || ($this->isChanged('Provider', 2) && $provider !== 'file')
            || $returnID
        ) {
            $id = null;

            if ($link = $this->SourceLink) {
                $id = $this->parseYouTubeID($link);

                if ($id) {
                    $provider = 'youtube';
                } else {
                    $id = $this->parseVimeoID($link);

                    if ($id) {
                        $provider = 'vimeo';
                    }
                }
            } else if ($this->Mp4()->exists() || $this->Webm()->exists()) {
                $provider = 'file';
            }

            if ($returnID) {
                return $id;
            }

            $this->Provider = $provider;
            $this->SourceID = $id;
        }
    }

    public function parseYouTubeID($link): string|bool
    {
        $re = '/(?im)\b(?:https?:\/\/)?(?:w{3}\.)?youtu(?:be)?\.(?:com|be)\/(?:(?:\??v=?i?=?\/?)|watch\?vi?=|watch\?.*?&v=|embed\/|)([A-Z0-9_-]{11})\S*(?=\s|$)/';
        preg_match($re, $link, $matches);

        if (isset($matches[1])) {
            return $matches[1];
        }

        return false;
    }

    public function parseVimeoID($link): string|bool
    {
        $re = '%^https?:\/\/(?:www\.|player\.)?vimeo.com\/(?:channels\/(?:\w+\/)?|groups\/([^\/]*)\/videos\/|album\/(\d+)\/video\/|video\/|)(\d+)(?:$|\/|\#?)(?:[?]?.*)$%im';
        preg_match($re, $link, $matches);

        if (isset($matches[3])) {
            return $matches[3];
        }

        return false;
    }

    public function HasVideo()
    {
        if (
            ($this->Provider === 'file' && ($this->Mp4()->exists() || $this->Webm()->exists()))
            || ($this->Provider !== 'file' && $this->VideoID)
        ) {
            return true;
        }
    }
}
