<?php

namespace bhofstaetter\GCO;

use bhofstaetter\FrameworkTweaks\BooleanDropdownField;
use bhofstaetter\FrameworkTweaks\GridFieldConfig_Extended;
use bhofstaetter\FrameworkTweaks\NoticeField;
use bhofstaetter\FrameworkTweaks\Toolbox;
use Bummzack\SortableFile\Forms\SortableUploadField;
use SilverStripe\AssetAdmin\Forms\UploadField;
use SilverStripe\Assets\File;
use SilverStripe\Assets\Image;
use SilverStripe\Core\ClassInfo;
use SilverStripe\Forms\DropdownField;
use SilverStripe\Forms\FieldList;
use SilverStripe\Forms\Form;
use SilverStripe\Forms\GridField\GridField;
use SilverStripe\Forms\GridField\GridFieldDetailForm;
use SilverStripe\Forms\HTMLEditor\HTMLEditorField;
use SilverStripe\Forms\TextField;
use SilverStripe\ORM\DataExtension;
use SilverStripe\ORM\FieldType\DBHTMLText;
use SilverStripe\ORM\FieldType\DBText;
use SilverStripe\ORM\ValidationResult;
use SilverStripe\View\ArrayData;

class Contents extends DataExtension
{
    private static string $images_folder_name = 'images';
    private static string $icon_folder_name = 'images/icons';
    private static string $files_folder_name = 'files';
    private static string $videos_folder_name = 'videos';
    private static string $links_folder_name = 'files/links';
    private static string $videos_preview_image_folder_name = 'videos/preview';

    private static int $images_max = 100;
    private static int $files_max = 100;
    private static int $videos_max = 100;
    private static int $links_max = 100;
    private static int $content_objects_max = 100;

    private static bool $show_name_enabled = false;
    private static bool $content_enabled = false;
    private static bool $images_enabled = false;
    private static bool $icon_enabled = false;
    private static bool $files_enabled = false;
    private static bool $videos_enabled = false;
    private static bool $links_enabled = false;
    private static bool $content_objects_enabled = false;
    private static bool $class_switch_enabled = false;
    private static bool $show_name_default = false;

    private static array $allowed_content_objects_classes = [];
    private static array $forbidden_content_objects_classes = [];

    private static array $db = [
        'Name' => 'Varchar(255)',
        'ShowName' => 'Boolean',
        'Content' => 'HTMLText',
        'IconType' => "Enum('none,font,image','none')",
        'IconCode' => 'Varchar(255)',
    ];

    private static array $has_one = [
        'IconImage' => Image::class,
    ];

    private static array $many_many = [
        'Images' => Image::class,
        'Files' => File::class,
        'Videos' => Video::class,
        'Links' => Link::class,
        'ContentObjects' => ContentObject::class,
    ];

    private static array $many_many_extraFields = [
        'Images' => [
            'SortOrder' => 'Int'
        ],
        'Files' => [
            'SortOrder' => 'Int'
        ],
        'Videos' => [
            'SortOrder' => 'Int'
        ],
        'Links' => [
            'SortOrder' => 'Int'
        ],
        'ContentObjects' => [
            'SortOrder' => 'Int'
        ],
    ];

    private static array $owns = [
        'Images',
        'Files',
        'Videos',
        'Links',
        'IconImage',
    ];

    private static array $required_fields = [
        'Name'
    ];

    private static array $searchable_fields = [
        'Name'
    ];

    public function validate(ValidationResult $result)
    {
        $skipValidation = false;

        if ($this->owner->hasMethod('skipGCOValidation')) {
            $this->owner->skipGCOValidation($skipValidation);
        }

        $this->owner->extend('skipGCOValidation', $skipValidation);

        if (!$skipValidation) {
            $this->validateAtLeastOneContentIsSet($result);
        }
    }

    private function validateAtLeastOneContentIsSet(ValidationResult $result)
    {
        // todo: respect enabled contents for validation (content, images, files, links)
        // todo: validate content objects

        if (
            !$this->owner->Content
            && !$this->owner->Images()->count() // todo: include draft
            && !$this->owner->Files()->count() // todo: include draft
            && !$this->owner->Links()->count() // todo: include draft
            && !$this->validateIcon()
            && $this->validateVideos()
            && $this->validateLinks()
        ) {
            $result->addError(_t(ContentObject::class . '.NoContentError', 'At least one content (image, text, ...) is necessary'));
        }
    }

    private function validateIcon()
    {
        return $this->owner->IconType !== 'none' && ($this->owner->IconCode || $this->owner->IconImage()->exists());  // todo: include draft
    }

    private function validateVideos()
    {
        if ($this->owner->ID) {
            return !$this->getVideosEnabled() || !$this->owner->Videos()->count();  // todo: include draft
        } else {
            if (!$this->getImagesEnabled() && !$this->getFilesEnabled() && !$this->getContentEnabled() && !$this->getLinksEnabled() && !$this->getIconEnabled()) {
                return false;
            }

            return true;
        }
    }

    private function validateLinks()
    {
        if ($this->owner->ID) {
            return !$this->getLinksEnabled() || !$this->owner->Links()->count();  // todo: include draft
        } else {
            if (!$this->getImagesEnabled() && !$this->getFilesEnabled() && !$this->getContentEnabled() && !$this->getVideosEnabled() && !$this->getIconEnabled()) {
                return false;
            }

            return true;
        }
    }

    public function canCreate($member = null, $context = [])
    {
        if ($this->owner->ClassName === ContentObject::class) {
            return false;
        }
    }

    public function populateDefaults()
    {
        parent::populateDefaults();

        $this->owner->ShowName = $this->getShowNameDefault();
    }

    public function onBeforeWrite()
    {
        parent::onBeforeWrite();

        if (
            !$this->getShowNameEnabled()
            && $this->owner->ShowName != $this->getShowNameDefault()
        ) {
            $this->owner->ShowName = $this->getShowNameDefault();
        }
    }

    public function updateCMSFields(FieldList $fields)
    {
        $fields->addFieldsToTab('Root.Main', [
            DropdownField::create('IconType', $this->getIconTypeLabel(), $this->owner->dbObject('IconType')->niceEnumValues()),
            $iconCode = TextField::create('IconCode', $this->getIconCodeLabel())
                ->setDescription(_t(ContentObject::class . '.IconCodeDescription', 'for example "fa-solid fa-star"')),
            $iconImage = UploadField::create('IconImage', $this->getIconImageLabel())
                ->setFolderName($this->getIconFolderName())
                ->setAllowedExtensions(['png', 'webp', 'gif', 'svg']),
            SortableUploadField::create('Images', $this->getImagesLabel())
                ->setFolderName($this->getImagesFolderName())
                ->setAllowedMaxFileNumber($this->getImagesMax()),
            SortableUploadField::create('Files', $this->getFilesLabel())
                ->setFolderName($this->getFilesFolderName())
                ->setAllowedMaxFileNumber($this->getFilesMax()),
            HTMLEditorField::create('Content', $this->getContentLabel())
                ->setRows(15),
            NoticeField::create(
                'VideosNotice',
                _t(ContentObject::class . '.VideosCreateNotice', 'After creating, a video can be uploaded|After creating, {count} videos can be uploaded', ['count' => $this->getVideosMax()]),
                $this->owner->ID || !$this->getVideosEnabled(),
                NoticeField::NOTICE,
                $this->getVideosLabel()
            ),
            $this->getVideosGridField(),
            NoticeField::create(
                'LinksNotice',
                _t(ContentObject::class . '.LinksCreateNotice', 'After creating, a link can be added|After creating, {count} links can be added', ['count' => $this->getVideosMax()]),
                $this->owner->ID || !$this->getLinksEnabled(),
                NoticeField::NOTICE,
                $this->getLinksLabel()
            ),
            $this->getLinksGridField(),
            NoticeField::create(
                'ContentObjectsNotice',
                _t(ContentObject::class . '.ContentObjectsCreateNotice', 'After creating, a content can be added|After creating, {count} contents can be added', ['count' => $this->getContentObjectsMax()]),
                $this->owner->ID || !$this->getContentObjectsEnabled(),
                NoticeField::NOTICE,
                $this->getContentObjectsLabel()
            ),
            $this->getContentObjectsGridField(),
        ]);

        $fields->addFieldsToTab('Root.Settings', [
            BooleanDropdownField::create('ShowName', $this->getLabelForField('show_name', 'ShowName', 'Display name'))
                ->setDescription(_t(ContentObject::class . '.ShowNameDescription', 'Should the name be displayed to the visitors of the website?'))
        ]);

        $iconCode->displayIf('IconType')->isEqualTo('font');
        $iconImage->displayIf('IconType')->isEqualTo('image');

        if (!$this->owner->ID || !$this->getVideosEnabled()) {
            $fields->removeByName('Videos');
        }

        if (!$this->owner->ID || !$this->getLinksEnabled()) {
            $fields->removeByName('Links');
        }

        if (!$this->getImagesEnabled()) {
            $fields->removeByName('Images');
        }

        if (!$this->getIconEnabled()) {
            $fields->removeByName('IconType');
            $fields->removeByName('IconCode');
            $fields->removeByName('IconImage');
        }

        if (!$this->getFilesEnabled()) {
            $fields->removeByName('Files');
        }

        if (!$this->getContentEnabled()) {
            $fields->removeByName('Content');
        }

        if (!$this->getShowNameEnabled()) {
            $fields->removeByName('ShowName');
        }

        if (!$this->getClassSwitchEnabled()) {
            $fields->removeByName('ClassName');
        }

        if (!$this->getContentObjectsEnabled()) {
            $fields->removeByName('ContentObjects');
        }

        $this->owner->extend('afterGCOCMSFields', $fields);
    }

    public function getVideosGridField()
    {
        $field = GridField::create('Videos', $this->getVideosLabel(), $this->owner->Videos());

        $config = GridFieldConfig_Extended::create($field, 'SortOrder');
        $config->enableRelationEditor();
        $config->enableVersioning();

        $config->modifyEditForm(function ($record, $fields, Form $form, $itemRequest) {
            $fields->dataFieldByName('Mp4')->setFolderName($this->getVideosFolderName());
            $fields->dataFieldByName('Webm')->setFolderName($this->getVideosFolderName());
            $fields->dataFieldByName('PreviewImage')->setFolderName($this->getVideosPreviewImageFolderName());
        });

        if ($this->owner->Videos()->count() >= $this->getVideosMax()) {
            $config->disable(['addNew', 'addExisting']);
            $config
                ->getComponentByType(GridFieldDetailForm::class)
                ->setShowAdd(false);
        }

        $field->setConfig($config);

        return $field;
    }

    public function getLinksGridField()
    {
        $field = GridField::create('Links', $this->getLinksLabel(), $this->owner->Links());

        $config = GridFieldConfig_Extended::create($field, 'SortOrder');
        $config->enableRelationEditor();
        $config->enableVersioning();

        $config->modifyEditForm(function ($record, $fields, Form $form, $itemRequest) {
            $fields->dataFieldByName('File')->setFolderName($this->getLinksFolderName());
        });

        if ($this->owner->Links()->count() >= $this->getLinksMax()) {
            $config->disable(['addNew', 'addExisting']);
            $config
                ->getComponentByType(GridFieldDetailForm::class)
                ->setShowAdd(false);
        }

        $field->setConfig($config);

        return $field;
    }

    public function getContentObjectsGridField()
    {
        $field = GridField::create('ContentObjects', $this->getContentObjectsLabel(), $this->owner->ContentObjects());

        $config = GridFieldConfig_Extended::create($field, 'SortOrder');
        $config->enableRelationEditor();
        $config->enableVersioning();

        $config->enableMultiClass($this->getEnabledContentObjectsClasses());

        if ($this->owner->ContentObjects()->count() >= $this->getContentObjectsMax()) {
            $config->disable(['addNew', 'addExisting']);
            $config
                ->getComponentByType(GridFieldDetailForm::class)
                ->setShowAdd(false);
        }

        $field->setConfig($config);

        return $field;
    }

    public function getEnabledContentObjectsClasses() {
        $allowed = $this->getAllowedContentObjectsClasses();
        $forbidden = $this->getForbiddenContentObjectsClasses();
        return array_diff($allowed, $forbidden);
    }

    public function getShowNameEnabled()
    {
        return $this->getUpdatableConfigValue('show_name_enabled', 'ShowNameEnabled');
    }

    public function getContentEnabled()
    {
        return $this->getUpdatableConfigValue('content_enabled', 'ContentEnabled');
    }

    public function getIconEnabled()
    {
        return $this->getUpdatableConfigValue('icon_enabled', 'IconEnabled');
    }

    public function getImagesEnabled()
    {
        $enabled = $this->getUpdatableConfigValue('images_enabled', 'ImagesEnabled');

        if ($enabled && !$this->getImagesMax()) {
            return false;
        }

        return $enabled;
    }

    public function getFilesEnabled()
    {
        $enabled = $this->getUpdatableConfigValue('files_enabled', 'FilesEnabled');

        if ($enabled && !$this->getFilesMax()) {
            return false;
        }

        return $enabled;
    }

    public function getVideosEnabled()
    {
        $enabled = $this->getUpdatableConfigValue('videos_enabled', 'VideosEnabled');

        if ($enabled && !$this->getVideosMax()) {
            return false;
        }

        return $enabled;
    }

    public function getLinksEnabled()
    {
        $enabled = $this->getUpdatableConfigValue('links_enabled', 'LinksEnabled');

        if ($enabled && !$this->getLinksMax()) {
            return false;
        }

        return $enabled;
    }

    public function getContentObjectsEnabled()
    {
        $enabled = $this->getUpdatableConfigValue('content_objects_enabled', 'ContentObjectsEnabled');

        if ($enabled && (!$this->getContentObjectsMax() || !count($this->getEnabledContentObjectsClasses()))) {
            return false;
        }

        return $enabled;
    }

    public function getClassSwitchEnabled()
    {
        return $this->getUpdatableConfigValue('class_switch_enabled', 'ClassSwitchEnabled');
    }

    public function getAllowedContentObjectsClasses()
    {
        $classes = $this->getUpdatableConfigValue('allowed_content_objects_classes', 'AllowedContentObjectsClasses');

        if (!$classes || (count($classes) === 1 && $classes[array_key_first($classes)] === ContentObject::class)) {
            $classes = ClassInfo::subclassesFor(ContentObject::class, false);
        }

        return (array) $classes;
    }

    public function getForbiddenContentObjectsClasses()
    {
        return (array) $this->getUpdatableConfigValue('forbidden_content_objects_classes', 'ForbiddenContentObjectsClasses');
    }

    public function getContentLabel()
    {
        return $this->getLabelForField('content_label', 'ContentLabel', 'Content');
    }

    public function getImagesLabel()
    {
        return $this->getLabelForField('images_label', 'ImagesLabel', 'Image|Images (max {count})', $this->getImagesMax());
    }

    public function getFilesLabel()
    {
        return $this->getLabelForField('files_label', 'FilesLabel', 'File|Files (max {count})', $this->getFilesMax());
    }

    public function getVideosLabel()
    {
        return $this->getLabelForField('videos_label', 'VideosLabel', 'Video|Videos (max {count})', $this->getVideosMax());
    }

    public function getLinksLabel()
    {
        return $this->getLabelForField('links_label', 'LinksLabel', 'Video|Links (max {count})', $this->getFilesMax());
    }

    public function getIconTypeLabel()
    {
        return $this->getLabelForField('icon_type_label', 'IconTypeLabel', 'Icon type');
    }

    public function getIconCodeLabel()
    {
        return $this->getLabelForField('icon_code_label', 'IconCodeLabel', 'Icon code');
    }

    public function getIconImageLabel()
    {
        return $this->getLabelForField('icon_image_label', 'IconImageLabel', 'Icon');
    }

    public function getContentObjectsLabel()
    {
        return $this->getLabelForField('content_objects_label', 'ContentObjectsLabel', 'Inhalt|Inhalte (max {count})', $this->getContentObjectsMax());
    }

    public function getImagesFolderName()
    {
        return $this->getUpdatableConfigValue('images_folder_name', 'ImagesFolderName');
    }

    public function getIconFolderName()
    {
        return $this->getUpdatableConfigValue('icon_folder_name', 'IconFolderName');
    }

    public function getFilesFolderName()
    {
        return $this->getUpdatableConfigValue('files_folder_name', 'FilesFolderName');
    }

    public function getVideosFolderName()
    {
        return $this->getUpdatableConfigValue('videos_folder_name', 'VideosFolderName');
    }

    public function getVideosPreviewImageFolderName()
    {
        return $this->getUpdatableConfigValue('videos_preview_image_folder_name', 'VideosPreviewImageFolderName');
    }

    public function getLinksFolderName()
    {
        return $this->getUpdatableConfigValue('links_folder_name', 'LinksFolderName');
    }

    public function getImagesMax()
    {
        return $this->getUpdatableConfigValue('images_max', 'ImagesMax');
    }

    public function getFilesMax()
    {
        return $this->getUpdatableConfigValue('files_max', 'FilesMax');
    }

    public function getVideosMax()
    {
        return $this->getUpdatableConfigValue('videos_max', 'VideosMax');
    }

    public function getLinksMax()
    {
        return $this->getUpdatableConfigValue('links_max', 'LinksMax');
    }

    public function getContentObjectsMax()
    {
        return $this->getUpdatableConfigValue('content_objects_max', 'ContentObjectsMax');
    }

    public function getShowNameDefault()
    {
        return $this->getUpdatableConfigValue('show_name_default', 'ShowNameDefault');
    }

    private function FirstObjectOrFalse($list)
    {
        return $list
            ? $list->first()
            : false;
    }

    public function SortedImages(string|int $limit = 0)
    {
        return $this->getImagesEnabled()
            ? $this->owner->Images()->sort('SortOrder')->limit($limit)
            : false;
    }

    public function Image()
    {
        return $this->FirstObjectOrFalse($this->SortedImages(1));
    }

    public function SortedFiles(string|int $limit = 0)
    {
        return $this->getFilesEnabled()
            ? $this->owner->Files()->sort('SortOrder')->limit($limit)
            : false;
    }

    public function File()
    {
        return $this->FirstObjectOrFalse($this->SortedFiles(1));
    }

    public function SortedVideos(string|int $limit = 0)
    {
        return $this->getVideosEnabled()
            ? $this->owner->Videos()->sort('SortOrder')->limit($limit)
            : false;
    }

    public function Video()
    {
        return $this->FirstObjectOrFalse($this->SortedVideos(1));
    }

    public function SortedLinks(string|int $limit = 0)
    {
        return $this->getLinksEnabled()
            ? $this->owner->Links()->sort('SortOrder')->limit($limit)
            : false;
    }

    public function FirstLink()
    {
        return $this->FirstObjectOrFalse($this->SortedLinks(1));
    }

    public function SortedContentObjects(string|int $limit = 0)
    {
        return $this->getContentObjectsEnabled()
            ? $this->owner->ContentObjects()->sort('SortOrder')->limit($limit)
            : false;
    }

    public function ContentObject()
    {
        return $this->FirstObjectOrFalse($this->SortedContentObjects(1));
    }

    public function Icon()
    {
        return ($this->getIconEnabled() && $this->owner->IconType !== 'none' && ($this->owner->IconCode || $this->owner->IconImage()->exists()))
            ? ArrayData::create([
                'Type' => $this->owner->IconType,
                'Code' => DBHTMLText::create()->setValue('<i class="' . $this->owner->IconCode . '"></i>'),
                'Image' => $this->owner->IconImage(),
            ])
            : false;
    }

    public function getContent()
    {
        if (Toolbox::is_frontend()) {
            return $this->getContentEnabled()
                ? $this->owner->getField('Content')
                : false;
        } else {
            return $this->owner->getField('Content');
        }
    }

    private function getLabelForField(string $configKey, string $labelKey, string $defaultLabel, int $count = 0)
    {
        $label = $this->owner->config()->get($configKey);

        if (!$label) {
            $label = $count
                ? _t(ContentObject::class . '.' . $labelKey, $defaultLabel, ['count' => $count])
                : _t(ContentObject::class . '.' . $labelKey, $defaultLabel);
        }

        $this->owner->extend('update' . $labelKey, $label);

        return $label;
    }

    private function getUpdatableConfigValue($configKey, $configName)
    {
        if (!$this->owner) {
            return;
        }

        $value = $this->owner->config()->get($configKey);
        $methodName = 'update' . $configName;

        if ($this->owner->hasMethod($methodName)) {
            $this->owner->$methodName($value);
        }

        $this->owner->extend($methodName, $value);

        return $value;
    }
}
