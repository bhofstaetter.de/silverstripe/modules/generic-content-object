<?php

namespace bhofstaetter\GCO;

use SilverStripe\Admin\ModelAdmin;
use SilverStripe\Forms\GridField\GridField;

class ContentAdmin extends ModelAdmin {

    private static $menu_title = 'Inhalte';
    private static $url_segment = 'contents';

    private static array $managed_models = [
        ContentObject::class,
    ];

    protected function getGridField(): GridField
    {
        $field = parent::getGridField();
        return $field;
    }
}
