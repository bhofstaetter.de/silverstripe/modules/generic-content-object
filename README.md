# Generic Content Object

Description

## Installation

```composer require bhofstaetter/generic-content-object```

## Configuration

T.B.D.

## Content Object

- ``$Title``
- ``$Content``
- ``$Icon`` with ``$Icon.Type``, ``$Icon.Code``, ``$Icon.Image``
- ``$SortedLinks`` or ``$Link``
- ``$SortedVideos`` or ``$Video``
- ``$SortedImages`` or ``$Image``
- ``$SortedFiles`` or ``$File``
- 

## Todo


- todo extended gridfield: bestehenden eintrag hinzufügen ui geht nicht
- translate oder .nice .count in framework tweaks labels zeugs aufnhemen (summary fields) = hä?!

- getLabel methode mit übersetzung singular/pluralr verbessern + defaults
- framework tweaks canUnlink / updateCanUnlink verwendung besser erklären
- framework tweaks duplciate check IGNORE_EMPTY in Readme
- translations of dataobject names
- model admin live template + more defaults in tweaks?
- framework tweaks, bulk actions für gridfields auch im modeladmin

  // todo: wenn nur ein Video, dann sowas wie has_one_button
  // Icon // todo => FA Picker Module
  // todo: booleandropdownfield default wird nicht gesetzt
  
- translate video
- class switcher
- content object validate DRAFT problem

